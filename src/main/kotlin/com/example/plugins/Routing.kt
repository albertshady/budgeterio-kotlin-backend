package com.example.plugins

import com.example.dao.dao
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.freemarker.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*

fun Application.configureRouting() {
    routing {
        get("/api/v1/accounts/") {
            val accounts = dao.allAccounts()
            call.respond(accounts)
        }

        post("api/v1/accounts/") {
            val formParameters = call.receiveParameters()
            val name = formParameters.getOrFail("name")
            val initialBalance = formParameters.getOrFail("initialBalance")
            val account = dao.addNewAccount(name, initialBalance.toDouble())!!
            call.respond(account)
        }

        put("api/v1/accounts/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            val name = formParameters.getOrFail("name")
            val initialBalance = formParameters.getOrFail<Double>("initialBalance")
            val account = dao.editAccount(id = id, name = name, initialBalance = initialBalance)!!
            call.respond(account)
        }

        delete("api/v1/accounts/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val deleted = dao.deleteAccount(id = id)

            if (!deleted) {
                call.respond(status = HttpStatusCode.BadRequest, message = "Not deleted")
            } else {
                call.respond(status = HttpStatusCode.OK, message = "Ok")
            }
        }

        get("/api/v1/categories/") {
            val categories = dao.allCategories()
            call.respond(categories)
        }

        post("api/v1/categories/") {
            val formParameters = call.receiveParameters()
            val name = formParameters.getOrFail("name")
            val category = dao.addNewCategory(name)!!
            call.respond(category)
        }

        put("api/v1/categories/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            val name = formParameters.getOrFail("name")
            val category = dao.editCategory(id = id, name = name)!!
            call.respond(category)
        }

        delete("api/v1/categories/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val deleted = dao.deleteCategory(id = id)

            if (!deleted) {
                call.respond(status = HttpStatusCode.BadRequest, message = "Not deleted")
            } else {
                call.respond(status = HttpStatusCode.OK, message = "Ok")
            }
        }

        get("/api/v1/transactions/") {
            val transactions = dao.allTransactions()
            call.respond(transactions)
        }

        post("api/v1/transactions/") {
            val formParameters = call.receiveParameters()
            val amount = formParameters.getOrFail<Double>("amount")
            val description = formParameters.getOrFail("description")
            val timestamp = formParameters.getOrFail<Int>("timestamp")
            val categoryId = formParameters.getOrFail<Int>("categoryId")
            val accountId = formParameters.getOrFail<Int>("accountId")
            val transaction = dao.addNewTransaction(
                amount = amount,
                description = description,
                timestamp = timestamp,
                categoryId = categoryId,
                accountId = accountId
            )!!
            call.respond(transaction)
        }

        put("api/v1/transactions/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            val amount = formParameters.getOrFail<Double>("amount")
            val description = formParameters.getOrFail("description")
            val timestamp = formParameters.getOrFail<Int>("timestamp")
            val categoryId = formParameters.getOrFail<Int>("categoryId")
            val accountId = formParameters.getOrFail<Int>("accountId")
            val transaction = dao.editTransaction(
                id = id,
                amount = amount,
                description = description,
                timestamp = timestamp,
                categoryId = categoryId,
                accountId = accountId
            )!!
            call.respond(transaction)
        }

        delete("api/v1/transactions/{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val deleted = dao.deleteTransaction(id = id)

            if (!deleted) {
                call.respond(status = HttpStatusCode.BadRequest, message = "Not deleted")
            } else {
                call.respond(status = HttpStatusCode.OK, message = "Ok")
            }
        }
    }
}
