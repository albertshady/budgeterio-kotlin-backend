package com.example.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*

@Serializable
data class Transaction(
    val id: Int,
    val amount: Double,
    val timestamp: Int,
    val description: String,
    val categoryId: Int,
    val accountId: Int
)

object Transactions : Table() {
    val id = integer("id").autoIncrement()
    val amount = double("amount")
    val timestamp = integer("timestamp")
    val description = varchar("description", 128)
    val categoryId = integer("category_id") references Categories.id
    val accountId = integer("account_id") references Accounts.id

    override val primaryKey = PrimaryKey(id)
}
