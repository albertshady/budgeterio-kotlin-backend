package com.example.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*

@Serializable
data class Account(val id: Int, val name: String, val initialBalance: Double, val transactionSum: Double)

object Accounts : Table() {
    val id = integer("id").autoIncrement()
    val name = varchar("name", 128).uniqueIndex()
    val initialBalance = double("initialBalance")

    override val primaryKey = PrimaryKey(id)
}
