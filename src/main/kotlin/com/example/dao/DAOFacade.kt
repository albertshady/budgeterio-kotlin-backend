package com.example.dao

import com.example.models.*

interface DAOFacade {
    suspend fun allAccounts(): List<Account>
    suspend fun account(id: Int): Account?
    suspend fun addNewAccount(name: String, initialBalance: Double): Account?
    suspend fun editAccount(id: Int, name: String, initialBalance: Double): Account?
    suspend fun deleteAccount(id: Int): Boolean

    suspend fun allCategories(): List<Category>
    suspend fun category(id: Int): Category?
    suspend fun addNewCategory(name: String): Category?
    suspend fun editCategory(id: Int, name: String): Category?
    suspend fun deleteCategory(id: Int): Boolean

    suspend fun allTransactions(): List<Transaction>
    suspend fun transaction(id: Int): Transaction?
    suspend fun addNewTransaction(
        amount: Double,
        description: String,
        timestamp: Int,
        accountId: Int,
        categoryId: Int
    ): Transaction?
    suspend fun editTransaction(
        id: Int,
        amount: Double,
        description: String,
        timestamp: Int,
        accountId: Int,
        categoryId: Int
    ): Transaction?

    suspend fun deleteTransaction(id: Int): Boolean
}

