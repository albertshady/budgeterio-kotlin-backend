package com.example.dao

import com.example.dao.DatabaseFactory.dbQuery
import com.example.models.*
import com.example.models.Transaction
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class DAOFacadeImpl : DAOFacade {
    private fun resultRowToAccount(row: ResultRow) =
        Account(
            id = row[Accounts.id],
            name = row[Accounts.name],
            initialBalance = row[Accounts.initialBalance],
            transactionSum = row[Transactions.amount.sum()] ?: 0.0
        )

    private fun resultRowToCategory(row: ResultRow) =
        Category(
            id = row[Categories.id],
            name = row[Categories.name],
        )

    private fun resultRowToTransaction(row: ResultRow) =
        Transaction(
            id = row[Transactions.id],
            amount = row[Transactions.amount],
            timestamp = row[Transactions.timestamp],
            description = row[Transactions.description],
            accountId = row[Transactions.accountId],
            categoryId = row[Transactions.categoryId],
        )

    override suspend fun allAccounts(): List<Account> = dbQuery {
        Accounts.join(Transactions, JoinType.INNER, additionalConstraint = { Accounts.id eq Transactions.accountId })
            .slice(Accounts.id, Accounts.name, Accounts.initialBalance, Transactions.amount.sum()).selectAll()
            .groupBy(Accounts.id)
            .map(::resultRowToAccount)
    }

    override suspend fun account(id: Int): Account? = dbQuery {
        (Accounts innerJoin Transactions).slice(
            Accounts.id,
            Accounts.name,
            Accounts.initialBalance,
            Transactions.amount.sum()
        ).select(Accounts.id eq Transactions.accountId and (Accounts.id eq id)).groupBy(Accounts.id)
            .map(::resultRowToAccount).singleOrNull()
    }

    override suspend fun addNewAccount(name: String, initialBalance: Double): Account? = dbQuery {
        val insertStatement =
            Accounts.insert {
                it[Accounts.name] = name
                it[Accounts.initialBalance] = initialBalance
            }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToAccount)
    }

    override suspend fun editAccount(id: Int, name: String, initialBalance: Double): Account? {
        dbQuery {
            Accounts.update({ Accounts.id eq id }) {
                it[Accounts.name] = name
                it[Accounts.initialBalance] = initialBalance
            }
        }
        return account(id = id)
    }

    override suspend fun deleteAccount(id: Int): Boolean = dbQuery {
        Accounts.deleteWhere { Accounts.id eq id } > 0
    }

    override suspend fun allCategories(): List<Category> = dbQuery {
        Categories.selectAll().map(::resultRowToCategory)
    }

    override suspend fun category(id: Int): Category? = dbQuery {
        Categories.select { Categories.id eq id }.map(::resultRowToCategory).singleOrNull()
    }

    override suspend fun addNewCategory(name: String): Category? = dbQuery {
        val insertStatement =
            Categories.insert {
                it[Categories.name] = name
            }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToCategory)
    }

    override suspend fun editCategory(id: Int, name: String): Category? {
        dbQuery {
            Categories.update({ Categories.id eq id }) {
                it[Categories.name] = name
            }
        }
        return category(id = id)
    }

    override suspend fun deleteCategory(id: Int): Boolean = dbQuery {
        Categories.deleteWhere { Categories.id eq id } > 0
    }

    override suspend fun allTransactions(): List<Transaction> = dbQuery {
        Transactions.selectAll().map(::resultRowToTransaction)
    }

    override suspend fun transaction(id: Int): Transaction? = dbQuery {
        Transactions.select { Transactions.id eq id }.map(::resultRowToTransaction).singleOrNull()
    }

    override suspend fun addNewTransaction(
        amount: Double,
        description: String,
        timestamp: Int,
        accountId: Int,
        categoryId: Int
    ): Transaction? = dbQuery {
        val insertStatement =
            Transactions.insert {
                it[Transactions.amount] = amount
                it[Transactions.description] = description
                it[Transactions.timestamp] = timestamp
                it[Transactions.accountId] = accountId
                it[Transactions.categoryId] = categoryId
            }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToTransaction)
    }

    override suspend fun editTransaction(
        id: Int,
        amount: Double,
        description: String,
        timestamp: Int,
        accountId: Int,
        categoryId: Int
    ): Transaction? {
        dbQuery {
            Transactions.update({ Transactions.id eq id }) {
                it[Transactions.amount] = amount
                it[Transactions.description] = description
                it[Transactions.timestamp] = timestamp
                it[Transactions.accountId] = accountId
                it[Transactions.categoryId] = categoryId
            }
        }
        return transaction(id = id)
    }

    override suspend fun deleteTransaction(id: Int): Boolean = dbQuery {
        Transactions.deleteWhere { Transactions.id eq id } > 0
    }
}

val dao: DAOFacade =
    DAOFacadeImpl().apply {
        runBlocking {
            if (allAccounts().isEmpty()) {
                addNewAccount("cash", 0.0)
            }
            if (allCategories().isEmpty()) {
                addNewCategory("food")
            }
        }
    }

